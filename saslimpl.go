//go:build sasl
// +build sasl

package mgo

import (
	"gitlab.com/wangzhuan/github.com.globalsign.mgo/internal/sasl"
)

func saslNew(cred Credential, host string) (saslStepper, error) {
	return sasl.New(cred.Username, cred.Password, cred.Mechanism, cred.Service, host)
}
